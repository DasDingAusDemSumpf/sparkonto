package de.templin.user;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;

import de.templin.connection.sqlconnection;

public class conf {
	
	public static String adduser(String name) {
		
		if(checkname(name)<=0) {
			try(Connection conn = sqlconnection.connect()){
				
				PreparedStatement prep = conn.prepareStatement("insert into user values (?,?);");
			
				prep.setString(2, name);
				prep.addBatch();
		    
				conn.setAutoCommit(false);
				prep.executeBatch();
				conn.setAutoCommit(true);

	        }catch (SQLException e) {
	        	System.out.println(e.getMessage());
	        }
			return "Benutzer angelegt";
		}
		return "Benutzername ungueltig";
	}
	
	public static int checkname(String name) {
		String sql = "SELECT id, name FROM user";
		try(Connection conn = sqlconnection.connect(); 
			Statement stmt = conn.createStatement();
			ResultSet rs = stmt.executeQuery(sql)){
			name=name.toUpperCase();
            while(rs.next()) {
            	if(name.equals(rs.getString("name").toUpperCase())) {
            		System.out.println("Exit");
            		return 1;
            	}
            }
			
		}catch (SQLException e) {
        	System.out.println(e.getMessage());
        }
		return 0;
	}
}
