package de.templin.connection;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;

public class sqlconnection {
	
	public static Connection connect() throws SQLException {
		
        String url = "jdbc:sqlite:src/main/resources/sparkonten.sqlite";
        Connection conn = null;
        try {
            conn = DriverManager.getConnection(url);
        } catch (SQLException e) {
            System.out.println(e.getMessage());
        } 
       return conn;
	}

}
